<?php 

include('header.inc');
$array = $redis->keys('photos*');
sort($array);
$array = array_reverse($array);
$active=0;

foreach($array as $photo_id){
    $photo_val = $redis->hgetall($photo_id);
    if(isset($photo_val['link']) && $photo_val['link'] != '' && $photo_val['status'] == 1){
        $active++;
    }
}
switch ($active) {
        case '1':
            $class = 'justone';
            break;

        case '2':
            $class = 'justtwo';
            break;
        
        default:
            $class = '';
            break;
    }

$signup = '<div class="flex-column flex-large">
                <div class="newsletter">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-12">
                                <p>Like quick recipes, contests and coupons? Sign up below for our newsletters!</p>
                                <form id="ic_signupform" captcha-key="6LeCZCcUAAAAALhxcQ5fN80W6Wa2K3GqRQK6WRjA" captcha-theme="light" new-captcha="true" method="POST" action="https://app.icontact.com/icp/core/mycontacts/signup/designer/form/?id=98&cid=1335852&lid=2943" class="cloverleaf-form">
                                    <div class="input-group mb-2 submit-container">
                                        <input type="text" tabindex=2 id="sub-email" class="form-control" placeholder="Email Address" name=data[email]>
                                        <div class="input-group-append">
                                            <input type="submit" id="signup_button" class="submit-btn" value="Submit">
                                        </div>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" value="1" id="sub-confirm" class="form-check-input" tabindex=1 name="data[confirmsubscription][]">
                                        <input type="hidden" value="1" name="data[acknowledgeprivacypolicy][]">

                                        <label class="form-check-label" for="sub-confirm"><p>*I have entered my email address to receive Clover Leaf Seafood\'s monthly newsletter containing updates and promotions regarding recipes, products, health & wellness and sustainability, in addition to other emails of news-worthy information. I am aware I can unsubscribe at any time.  I acknowledge that I have read and understand the <a href="https://www.cloverleaf.ca/en/privacy-policy" target="_blank" >privacy policy</a> and agree to its terms.</p>
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            ';

?>

<h3>Tap an image for more details</h3>
<div class="container">
    <div class="row">
        <div class="flex-container">
            <?php
                if(!empty($array)) {

                    $count = 0;
                    $sign_up_shown = false;
                    foreach($array as $photo_id){
                        $photo_val = $redis->hgetall($photo_id);
                        if(isset($photo_val['link']) && $photo_val['link'] != '' && $photo_val['status'] == 1){
                            echo "<div class=\"$class flex-column\"><a alt='cloverleaf image pulled from IG' target='_blank' href=". $photo_val['link'] ."><img src=". $photo_val['size_full'] ."></a></div>";
                            if($count == 2 && !$sign_up_shown){
                                echo $signup;
                                $sign_up_shown = true;
                            }
                            $count++;
                        }
                    }

                    if(!$sign_up_shown){
                        echo $signup;
                    }

                } else {
                    echo "No posts found";
                }
            ?>
        </div>
    </div>
</div>
<img src="//app.icontact.com/icp/core/signup/tracking.gif?id=173&cid=1335852&lid=2943"/>

<?php include('footer.inc'); ?>
