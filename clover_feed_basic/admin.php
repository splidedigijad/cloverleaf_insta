<?php

if(isset($_GET['logout'])){
    $loggedIn = false;
    header('Location: index.php');
}

include('header.inc');

unset($message);

if(isset($_POST['email'])){
    $email = $redis->hmget('admin', 'email');
    $password = $redis->hmget('admin', 'password');

    if($email[0] == trim($_POST['email']) && $password[0] == trim($_POST['password'])) {
        $loggedIn = true;
        unset($message);
    } else {
        $loggedIn = false;
        $message = "Sorry the password and email do not match this account! Please try again.";
    }
}

//Check to see if user is logged in
if(isset($loggedIn) && $loggedIn == true) {
    $array = $redis->keys('photos*');
    sort($array);
    $array = array_reverse($array);
    ?>
    
    <div class="container">
        <div class="row">
            <div class="flex-container">
                <?php
                if(!empty($array)) {
                    $count = 0;
                    foreach($array as $photo_id){
                        $photo_val = $redis->hgetall($photo_id);
                        list($nil, $short_id) = explode(':', $photo_id);

                        if($photo_val['status'] == 1){

                            if(isset($photo_val['link'])) {
                                $photo_link = $photo_val['link'];
                            } else {
                                $photo_link = '';
                            }?>

                            <div class="flex-column">
                                <a href="#" data-toggle="modal" data-target="#link-modal-<?php echo $count; ?>">
                                    <img src="<?php echo $photo_val['size_full'] ?>">
                                </a>
                                <div class="modal fade" id="link-modal-<?php echo $count; ?>" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Add Recipe Link </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="#" method="POST" id="<?php echo $short_id; ?>" name="<?php echo $short_id; ?>" class="links">
                                                    <input type="text" class="form-control mb-4" id="photo_link_<?php echo $short_id; ?>"
                                                           name="photo_link_<?php echo $short_id; ?>" value="<?php echo $photo_link; ?>" placeholder="Please include the http:// or https//">
                                                    <input type="hidden" id="photo_id_<?php echo $short_id; ?>" name="photo_id_<?php echo $short_id; ?>" value="<?php echo $short_id; ?>">
                                                    <button type="submit" class="clover-btn">Save</button>
                                                    <div class="link-success my-3"></div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $count++;
                        }
                    } ?>

                    <div class="container mt-5">
                        <div class="row justify-content-center">
                            <div class="col-xs-12">
                                <a href="admin.php?logout=true" class="clover-btn">Logout & Return</a>
                            </div>
                        </div>
                    </div>

                <?php } else {
                    echo "No posts found";
                } ?>

            </div>
        </div>
    </div>

<?php } else { ?>

    <div class="container">
        <div class="row">
            <div class="flex-container">
                <form id="cloverleaf-login" name="login-form" class="login-form" action="admin.php" method="POST" onsubmit="return formValidate()">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="text" name="password">
                    </div>
                    <button type="submit" class="clover-btn">Login</button>

                    <div id="error" class="error">
                        <p><?php echo $message; ?></p>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php }

include('footer.inc'); ?>