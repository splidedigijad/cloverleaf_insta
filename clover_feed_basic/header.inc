<?php

include('dbase.php');

?>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link href="css/main.css" rel="stylesheet">

        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Clover Leaf Seafoods</title>
</head>

<body>

    <header class="header">
        <div class="container px-4">
            <div class="row">
                <a href="admin.php" class="profile-logo">
                    <img src="img/cloverleaf.png" class="img-fluid">
                </a>
                <div class="profile-details">
                    <h2>Clover Leaf Seafoods</h2>
                    <a href="https://www.instagram.com/clover_leaf_seafoods">@Clover_Leaf_Seafoods</a>
                </div>
            </div>
        </div>
    </header>