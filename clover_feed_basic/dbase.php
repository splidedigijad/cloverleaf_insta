<?php

require "../vendor/predis/predis/autoload.php";

Predis\Autoloader::register();

$dev='192.168.0.30';
$staging = 'CLOVERLEAFFEED.WEBWORKINPROGRESS.COM';
$local = '127.0.0.1';

try {
    $redis = new Predis\Client();
    $redis = new Predis\Client(array(
        "scheme" => "tcp",
        "host" => $local,
        "port" => 6379
    ));
} catch (Exception $e) {
    die($e->getMessage());
}

