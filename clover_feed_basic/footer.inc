<footer>

</footer>

            <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

            <script>
                function formValidate(){
                    var email = document.forms["login-form"]["email"].value;
                    var password = document.forms["login-form"]["password"].value;

                    if (email == "" || password == "") {
                        var $message = "<p>Sorry no blank fields accepted</p>";
                        $('#error').html($message).show();
                        return false;
                    }
                }

                $('#signup_button').click(function(event) {
                    fail = 'no'
                    email = $('#sub-email');
                    confirm = $('#sub-confirm');
                    if (email.val() == "" ) {
                        $(email).focus();
                        event.preventDefault();
                        return false;
                    }
                    if (confirm.prop('checked') == false) {
                        $(confirm).focus();
                        event.preventDefault();
                        return false;
                    }
                })

                <?php if(isset($message)){
                    echo "$('#error').show();";
                } ?>

                <?php 
                    $root = $_SERVER['SERVER_NAME'];
                    // dev
                    if ($root == 'cloverlocal') {
                        $root = 'cloverapilocal:8001';
                    }
                    // staging
                    if ($root == 'cloverstage.webworkinprogress.com') {
                        $root = 'cloverapi.webworkinprogress.com';
                    }
                    // production
                    if ($root == 'cloverleafinstagram.com') {
                        $root = 'cloverapi.webworkinprogress.com';
                    }
                    ?>
                    
                $("form.links").submit(function( event ) {
                    event.preventDefault();
                    var link =''; var thisID =''; var test = '';
                    photo_id = this.id;
                    link_string_to_eval = "photo_link_"+photo_id;
                    link = eval('this.'+link_string_to_eval+'.value');

                    var obj = new Object();
                    obj.whichPhoto = photo_id;
                    obj.link = link;

                    $.ajax({
                        url: 'http://<?php echo $root; ?>/api/v1/save',
                        type: 'PUT',
                        dataType: 'json',
                        crossDomain: true,
                        data: obj,
                        success: function(data) {
                            console.log(data);
                            $('div.link-success').hide().html('<p>Success! Link updated.</p><p>Once you log out of the Admin Panel, click this Photo to see it in action!</p><p>Press ESC to return to the Admin Panel</p>').fadeIn('3000');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(ajaxOptions);
                            console.log(thrownError);
                            console.log(xhr);
                            event.preventDefault();
                        }
                    })
                });

                $(".modal").on("hidden.bs.modal", function(){
                    $("div.link-success").html("");
                });

                <?php
                    if ($_SERVER['PHP_SELF'] == '/admin.php' ) {
                        if (isset($_SERVER['HTTP_REFERER'])) {
                            $origin = trim(preg_replace('~([^/]+$)~ui', '', $_SERVER['HTTP_REFERER']), '/');
                        } else {
                            $origin = '';
                        }
                    echo "
                    $.ajax({
                        url: 'http://$root/api/v1/admin',
                        type: 'GET',
                        dataType: 'json',
                        crossDomain: true,
                        headers: {  'Access-Control-Allow-Origin': '".$origin."' },
                        xhrFields: {
                            withCredentials: true,
                        }
                    })"; 
                    }; 
                ?>

            </script>

            <?php
                if ($_SERVER['PHP_SELF'] != '/admin.php' ) {
echo "
<script type='text/javascript' src='//app.icontact.com/icp/static/form/javascripts/validation-captcha.js'></script>

<script type='text/javascript' src='//app.icontact.com/icp/static/form/javascripts/tracking.js'></script>
"; } ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133311911-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-133311911-1');
</script>

        </div>
    </body>
</html>